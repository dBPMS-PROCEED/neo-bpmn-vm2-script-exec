import { ScriptExecutionError, ScriptExecutor } from 'neo-bpmn-engine';
import { NodeVM, NodeVMOptions } from 'vm2';

export default function createScriptExecutor(options: NodeVMOptions = {}): ScriptExecutor {
  return {
    execute: async (
      processId: string,
      processInstanceId: string,
      scriptString: string,
      dependencies: { [key: string]: unknown }
    ) => {
      const getService = dependencies.getService
        ? (serviceName: string) => {
            const service = (dependencies as any).getService(serviceName);
            if (service) {
              return new Proxy(
                {},
                {
                  get: function(target: any, name: string) {
                    if (name in service && typeof service[name] === 'function') {
                      return (...args: any[]) => service[name](processId, processInstanceId, ...args);
                    }

                    return target[name];
                  }
                }
              );
            }

            return service;
          }
        : undefined;

      const deps = { ...options.sandbox, ...dependencies, getService, processId, processInstanceId };

      const vm = new NodeVM({ ...options, sandbox: deps, wrapper: 'none' });
      const asyncFn = vm.run(`return async function() { ${scriptString} };`, './');

      return new Promise((resolve, reject) => {
        asyncFn()
          .then((result: any) => resolve(result))
          .catch((err: Error) => reject(new ScriptExecutionError(JSON.stringify(err))));
      });
    },

    stop: (processId: string, processInstanceId: string) => {
      // Not implemented
    }
  };
}
