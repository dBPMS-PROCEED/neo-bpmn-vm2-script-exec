import { ScriptExecutionError, ScriptExecutor } from 'neo-bpmn-engine';
import createScriptExecutor from '../src/';
import { createMockLogger } from './mockLogger';

let scriptExecutor: ScriptExecutor;

beforeEach(() => {
  scriptExecutor = createScriptExecutor();
});

describe('default script execution', () => {
  it('script execution - should call the log functions', async () => {
    const log = createMockLogger();
    const variables: any = { x: 10, y: 5 };
    const variablesService = {
      setVar: jest.fn(),
      getVar: jest.fn().mockImplementation((_, __, name: string) => variables[name])
    };
    const getService = jest.fn().mockImplementation(name => {
      if (name === 'variables') return variablesService;
    });

    await scriptExecutor.execute(
      'myProcess',
      'p1',
      `
      const variables = getService('variables');
      log.info('Hello');
      log.info('Variables: ' + variables.getVar('x')  + ' ' + variables.getVar('y'));
      `,
      { log, getService }
    );

    expect(variablesService.getVar).toHaveBeenCalledWith('myProcess', 'p1', 'x');
    expect(variablesService.getVar).toHaveBeenCalledWith('myProcess', 'p1', 'y');
    expect(log.info).toHaveBeenCalledWith('Hello');
    expect(log.info).toHaveBeenCalledWith('Variables: 10 5');
  });

  it('should throw if script execution errors out', async () => {
    const log = createMockLogger();

    try {
      await scriptExecutor.execute(
        'myProcess',
        'p1',
        `
        // this is an error
        someError
        log.info('not an error');
        `,
        { log }
      );
    } catch (e) {
      expect(e).toBeInstanceOf(ScriptExecutionError);
    } finally {
      expect(log.info).not.toHaveBeenCalled();
    }
  });

  it('should be able to call a provided service', async () => {
    const log = createMockLogger();
    const emailService = {
      send: jest.fn()
    };
    const getService = jest.fn().mockImplementation(name => {
      if (name === 'com.proceed.email') return emailService;
    });

    await scriptExecutor.execute(
      'myProcess',
      'p1',
      `
      const emailService = getService('com.proceed.email');
      emailService.send('hello', 'world');
      `,
      { log, getService }
    );

    expect(emailService.send).toHaveBeenCalledWith('myProcess', 'p1', 'hello', 'world');
  });

  it('should get back returned variables', async () => {
    const log = createMockLogger();
    const variablesService = {
      setVar: jest.fn(),
      getVar: jest.fn().mockReturnValue(10)
    };
    const getService = jest.fn().mockImplementation(name => {
      if (name === 'variables') return variablesService;
    });

    const returnedValue = await scriptExecutor.execute(
      'myProcess',
      'p1',
      `
      const variables = getService('variables');
      return { x: variables.getVar('x') + 10 };
      `,
      { log, getService }
    );

    expect(variablesService.getVar).toHaveBeenCalledWith('myProcess', 'p1', 'x');
    expect(returnedValue).toEqual({ x: 20 });
  });

  it('shoud be able to access processId and processInstanceId from script', async () => {
    const log = createMockLogger();

    await scriptExecutor.execute(
      'myProcess',
      'p1',
      `
        log.info(processId, processInstanceId);
        `,
      { log }
    );

    expect(log.info).toHaveBeenCalledWith('myProcess', 'p1');
  });
});
