# Node vm2 Script Executor for neo-bpmn-engine

This module is an optional dependency for `neo-bpmn-engine` to execute javascript on NodeJS [vm2](https://www.npmjs.com/package/vm2) with security against quitting the process. Note however that the process can still be stalled using `while(true) { }`.

`npm install neo-bpmn-vm2-script-exec`

To add the executor to the Engine,

```js
import * as Engine from `neo-bpmn-engine`;
import createScriptExecutor from `neo-bpmn-vm2-script-exec`;

// Make sure its NodeJS env and set the executor
if (typeof window === 'undefined') {
  const options = { }; // refer vm2 docs for options

  Engine.provideScriptExecutor( createScriptExecutor(options) );
}
```

We only vm2 option that is overridden is, `wrapper` with a value `''`, since the BPMN expects the script to return a promise.
